package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class Mazo {

    Stack<Carta> cartas;

    public Mazo() {
        this.cartas = new Stack<>();
        this.generarCartas();
    }
    private void generarCartas(){
        HashMap<Integer,String> palo = new HashMap();
        palo.put(1,"Espada");
        palo.put(2,"Basto");
        palo.put(3,"Oro");
        palo.put(4,"Copa");

        for (int valor=1; valor<5; valor++ ){
            for (int num=1; num<13; num++){
                Carta carta = new Carta(num,palo.get(valor));
                this.agregarCarta(carta);
            }
        }
    }
    public void agregarCarta (Carta carta){
        this.cartas.push(carta);
    }
    public boolean quedanCartas(){ //true si quedan cartas
        return !this.cartas.empty();
    }
    public Carta darUnaCarta(){
        return this.cartas.pop();
    }


}
