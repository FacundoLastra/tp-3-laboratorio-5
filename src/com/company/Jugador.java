package com.company;
import java.util.*;

/**
 * Created by SkyDoo on 8/4/2018.
 */
public class Jugador extends Thread {

    String nombreJugador;
    int puntaje;
    List<Carta> cartas;
    Mesa mesa;

    public String getNombreJugador() {
        return nombreJugador;
    }

    public Jugador (String nombreJugador, Mesa mesa){

        this.nombreJugador=nombreJugador;
        this.puntaje=0;
        this.cartas = new ArrayList<>();
        this.mesa= mesa;
        
    }

    public int getPuntaje() {
        return puntaje;
    }

    public int carcularPuntaje(){

        for (Carta carta: this.cartas) {
            this.puntaje+=carta.getValordeCarta();
        }
        return this.puntaje;
    }
    public Carta getUltimaCartaRecibida(){
        return this.cartas.get(this.cartas.size()-1);
    }


    @Override
    public void run(){

        while (this.mesa.quedanCartasEnelMazo()){  ///el jugador consume mientras el repartidor tenga cartas

            this.cartas.add(this.mesa.darUnaCarta());
            this.mesa.CartaRecibida(this); ///informa al maso q el jugador recibio la carta, y este notifica al observador

            if(!this.mesa.rondaTerminada()){ /// pregunto si la ronda no termino
                try {
                    this.sleep(25); /// duermo al hilo para darle tiempo a los otros jugadores de consumir
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }
        System.out.println("El jugador "+nombreJugador + "Termino de jugar.");

    }



}
