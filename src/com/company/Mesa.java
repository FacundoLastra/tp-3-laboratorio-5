package com.company;
import java.util.Observable;
import java.util.Stack;

/**
 * Created by SkyDoo on 8/4/2018.
 */
public class Mesa extends Observable{

    Stack<Carta> cartasParaRepartir;
    int nRonda;
    int nJugadores;
    Repartidor repartidor;

    public Mesa(){
        this.cartasParaRepartir = new Stack<>();
        this.nRonda = 0;
        this.nJugadores = 0;
        this.repartidor= new Repartidor();
        this.addObserver(this.repartidor);
    }

    public void setnJugadores(int nJugadores) {
        this.nJugadores = nJugadores;
    }


    public synchronized Carta darUnaCarta(){
        Carta cartaRepartida = null;
        while (cartasParaRepartir.empty()){
            try{
                wait();
            }
            catch (InterruptedException e)
            {}
        }
        cartaRepartida= this.cartasParaRepartir.pop();

        return cartaRepartida;

    }
    public synchronized void CartaRecibida(Jugador jugador){

        setChanged();
        notifyObservers(jugador);
    }

    public boolean rondaTerminada(){
        return this.cartasParaRepartir.empty();
    }
    public boolean  quedanCartasEnelMazo(){ ///true si quedan cartas para repartir
        return this.repartidor.quedanCartasparaRepartir();
    }

    public synchronized void darCartasDelaRonda(){ ///funcion q reparte las cartas de la ronda

        if (this.quedanCartasEnelMazo() && this.rondaTerminada()) {

                this.cartasParaRepartir = repartidor.darCartasDeRonda(this.nJugadores);
                this.nRonda++;

                notifyAll();
        }
    }






}
