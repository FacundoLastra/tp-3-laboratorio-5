package com.company;

import java.util.HashMap;

public class Carta {

    int numero;
    String palo;


    public Carta(int valor,String palo) {
        this.numero = valor;
        this.palo= palo;

    }

    public int getValordeCarta(){
       int valorDelaCarta;
        valorDelaCarta=ValoresDeCarta.valorDePalo(this.palo);
        valorDelaCarta+=this.numero;
        return valorDelaCarta;
    }
    @Override
    public String toString(){
        return this.palo+" "+this.numero;
    }

}
