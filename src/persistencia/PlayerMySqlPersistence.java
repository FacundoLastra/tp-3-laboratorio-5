package persistencia;

import com.company.Jugador;

import java.sql.*;
import java.util.Properties;
import java.io.FileInputStream;

/**
 * Created by SkyDoo on 10/4/2018.
 */
public class PlayerMySqlPersistence {
    Connection conexion;
    Properties queries;
    static PlayerMySqlPersistence instance;

    public static PlayerMySqlPersistence getInstance() {
        if (instance == null) {
            instance = new PlayerMySqlPersistence();
        }
        return instance;
    }

    private PlayerMySqlPersistence() {
        try {

            queries = new Properties();
            queries.load(new FileInputStream("./conf/queries.properties"));

            Class.forName("com.mysql.jdbc.Driver");
            // Se establece la conexiÃ³n con la base de datos
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/mysql", "root", "");

            // Se crea la base de datos y la tabla si no existe
            Statement st = conexion.createStatement();
            st.execute("CREATE DATABASE /*!32312 IF NOT EXISTS*/`tp3lab5` /*!40100 DEFAULT CHARACTER SET latin1 */;");
            st.execute(""
                    + "CREATE TABLE IF NOT EXISTS `tp3lab5`.`ganadores` ("
                    + "  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,"
                    + "  `nombre` varchar(50) DEFAULT NULL,"
                    + "  `puntaje` int(4) DEFAULT NULL,"
                    + "  PRIMARY KEY (`id`)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            conexion.close();
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp3lab5", "root", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean persistirGanador(Jugador ganador){
        boolean respuesta= false;
        try {
            String sql = queries.getProperty("query.agregarGanador");
            PreparedStatement st = conexion.prepareStatement(sql);
            st.setString(1, ganador.getNombreJugador());
            st.setString(2, String.valueOf(ganador.getPuntaje()));
            st.execute();
            respuesta= true;

        } catch (SQLException e){
            e.printStackTrace();
        }
        return respuesta;
    }




}
