package com.company;

import persistencia.PlayerMySqlPersistence;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Mesa mesa = new Mesa();
        Jugador player1= new Jugador("Jorge",mesa);
        Jugador player2= new Jugador("Hernesto",mesa);
        Jugador player3= new Jugador("Carlos",mesa);
        Jugador player4= new Jugador("Federico",mesa);

        ArrayList<Jugador> jugadores = new ArrayList<>();
        jugadores.add(player1);
        jugadores.add(player2);
        jugadores.add(player3);
        jugadores.add(player4);

        mesa.setnJugadores(jugadores.size());

        Juego partida = new Juego(mesa,jugadores);

        Jugador ganador = partida.jugar();




        PlayerMySqlPersistence persistir = PlayerMySqlPersistence.getInstance();
       if (persistir.persistirGanador(ganador))
       {
           System.out.println("El ganador es "+ganador.nombreJugador+ " con un puntaje de "+ganador.puntaje+ " y fue persistido con exito en la base de datos");
       }else{
           System.out.println("Ocurrio un error en el momento de persistir");
       }


    }
}
