package com.company;

import java.util.HashMap;

/**
 * Created by SkyDoo on 9/4/2018.
 */
public class ValoresDeCarta {

    public static Integer valorDePalo(String palo){
        HashMap<String,Integer> valores = new HashMap();
        valores.put("Espada",1);
        valores.put("Basto",2);
        valores.put("Oro",3);
        valores.put("Copa",4);

        return valores.get(palo);
    }


}
