package com.company;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by SkyDoo on 9/4/2018.
 */
public class Juego  {

    Mesa mesa;
    List<Jugador> arrayJugadores;

    public Juego(Mesa mesa, List<Jugador> jugadores){

        this.arrayJugadores=jugadores;
        this.mesa= mesa;

    }

    public Jugador calcularGanador(){
        Jugador ganador= null;
        for (Jugador player: this.arrayJugadores
             ) {
            player.carcularPuntaje();
            if (ganador != null){
                if (ganador.puntaje < player.puntaje){
                    ganador=player;
                }
            }else{
                ganador=player;
            }

        }

        return ganador;
    }

    public Jugador jugar(){
        for (Jugador player: this.arrayJugadores ///inicio todos los hilos
             ) {
            player.start();
        }

        while(this.mesa.quedanCartasEnelMazo()){ /// reparto las cartas, para q los hilos empiezen a consumir, sale del ciclo si se queda sin cartas el repartidor

            this.mesa.darCartasDelaRonda();

        }

        if (!mesa.rondaTerminada()){ ///para darle tiempo a los hilos de terminar la ultima ronda, antes de calcular puntaje, en caso de ser necesario

            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return this.calcularGanador();
    }



}
