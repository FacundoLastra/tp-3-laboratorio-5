package com.company;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

public class Repartidor implements Observer {

    Mazo mazoDeCartas;

    public Repartidor(){
        this.mazoDeCartas= new Mazo();
    }


    public Stack<Carta> darCartasDeRonda(int nJugadores){
        Stack<Carta> cartasDeRonda = new Stack();
        int cantidadCartas=0;

        while(mazoDeCartas.quedanCartas() && cantidadCartas < nJugadores){ ///va a llegar un momento q no haya suficientes cartas para todos los jugadores, esto probocara el fin del juego y el comienzo del conteo del puntaje.
            cartasDeRonda.push(this.mazoDeCartas.darUnaCarta());
            cantidadCartas++;
        }

        return cartasDeRonda;
    }

    @Override
    public void update(Observable o, Object arg) {
        if ( arg instanceof Jugador){
            System.out.println("El Jugador "+((Jugador) arg).getNombreJugador() + "Recibio la Carta "+((Jugador) arg).getUltimaCartaRecibida().toString());
        }
    }

    public boolean quedanCartasparaRepartir(){ ///true si quedan cartas para repartir
        return this.mazoDeCartas.quedanCartas();
    }


}
