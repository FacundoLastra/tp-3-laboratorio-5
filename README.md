TP 3 Laboratorio 5


Persistencia Utilizada:

JDBC Utilizando como motor de base de datos MySql, debido a la necesidad de persistir informacion. Ademas se implemento el patron Singleton sobre la clase que crea la conexión a la base de datos para evitar multiples instancias de la misma.


En caso de Agregar dificultad, explicar la funcionalidad del juego.

No se agrego dificultad extra, se implemento una baraja española que es administrada por el repartidor, este da cartas en base a el numero de jugadores. Los jugadores (que son Threads) se pelean entre si para agarrar las cartas dadas por el repartidor, una vez que todos los jugadores tienen 1 carta, se da por terminada la ronda y el repartidor vuelve a repartir, y el ciclo se repite hasta que el repartidor se quede sin cartas en el mazo.

Los puntos se suman en base a la letra de la carta + el numero, para sumar se usa el siguiente sistema:

	Valor de letra + numero de carta.

	el valor de la letra es dado por un hashmap, es el siguiente:
		Espada => 1
        Basto  => 2
        Oro    => 3
        Copa   => 4


Una vez terminado de calcular los puntajes de cada jugador, se decide el ganador en base de quien tiene mas puntaje, y este se persiste en la base de datos.
